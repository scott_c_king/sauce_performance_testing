import pytest
from os import environ

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.remote.remote_connection import RemoteConnection

import urllib3
urllib3.disable_warnings()


DESKTOP_BROWSERS = [{'platform': 'macOS 10.15',
                     'browserName': 'chrome',
                     'version': 'latest',
                     'screenResolution': '1600x1200',
                     'name': 'SCKing Sauce Performance Test',
                     'build': 'SCKing Sauce Performance Test',
                     'build_tag': 'SCKing Sauce Performance Test',
                     'username': 'dragos_qa',
                     'accessKey': '674552f5-1157-427c-ae6f-4ffd07cf523a',
                     'tunnelIdentifier': 'scking-proxy-tunnel',
                     'extendedDebugging': 'True',
                     'capturePerformance': 'True'}]

@pytest.fixture(params=DESKTOP_BROWSERS)
def perf_driver(request):
    selenium_endpoint = ('https://ondemand.saucelabs.com:443/wd/hub')
    capabilities = dict()
    capabilities.update(request.param)
    browser = webdriver.Remote(command_executor=selenium_endpoint,
                               desired_capabilities=capabilities,
                               keep_alive=True)

    # This is specifically for SauceLabs plugin.
    # In case test fails after selenium session creation having this here will help track it down.
    if browser is not None:
        print("SauceOnDemandSessionID={} job-name={}".format(browser.session_id, 'SCKing Sauce Performance Test'))
    else:
        raise WebDriverException("Never created!")

    yield browser

    # Teardown starts here
    # report results
    # use the test result to send the pass/fail status to Sauce Labs
    sauce_result = "failed" if request.node.rep_call.failed else "passed"
    browser.execute_script("sauce:job-result={}".format(sauce_result))
    browser.quit()


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    # this sets the result as a test attribute for Sauce Labs reporting.
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set an report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)