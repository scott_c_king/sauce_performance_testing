import os
import pytest
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def test_valid_crentials_login(perf_driver):
    perf_driver.get('https://platform-dev10.dragos.services/#/login')
    perf_driver.find_element_by_id('username').send_keys('admin')
    perf_driver.find_element_by_id('password').send_keys('Dr@gosSyst3m')
    perf_driver.find_element_by_xpath('//*[@id="external-form"]/div[5]/div[1]').click()
    perf_driver.find_element_by_css_selector('#external-form > button').click()
    WebDriverWait(perf_driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "menu-controls")))
    # We're logged in...

    # Map Loader
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__map"]').click()
    
    # Asset Explorer
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__assetexplorer"]').click()
    
    # Data Explorer
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__dataexplorer"]').click()
    
    # Notification Manager
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__notificationmanager"]').click()
    
    # Content Manager
    # perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__map"]').click()
    
    # Baseline Manager
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__baselinemanager"]').click()
    
    # Report Manager
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__reportmanager"]').click()

    # Sensors
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__sensors"]').click()

    # Admin
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__admin"]').click()

    # Apps
    perf_driver.find_element_by_id('//*[@id="primary-nav__upper__icon-links__apps"]').click()
